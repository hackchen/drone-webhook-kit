### docker_webhook 配合drone的web自动更新service的image

### 使用条件

需要使用 root 账号运行本程序

drone的配置文件的设置，其他步骤请自行编写

```
- name: 触发部署
  image: plugins/webhook
  settings:
    urls: 
      from_secret: webhook_url # 在 drone 的 secret 里面设置的变量
    debug: true
```

### 使用方法

```
上面 webhook_url 设置的内容的定义

POST http://127.0.0.1:8080/:name/:secret/*registry
name 为服务名称 例如：drone_test
secret 为通讯密码 例如：test ，可以在配置里面修改
registry 为镜像前缀 例如：registry.cn-hongkong.aliyuncs.com/asdfasdfwe/drone_test
完整示例
http://127.0.0.1:8080/drone_test/test/registry.cn-hongkong.aliyuncs.com/asdfasdfwe/drone_test
```