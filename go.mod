module docker_webhook

go 1.15

require (
	github.com/gin-gonic/gin v1.7.2
	github.com/spf13/viper v1.8.1
)
